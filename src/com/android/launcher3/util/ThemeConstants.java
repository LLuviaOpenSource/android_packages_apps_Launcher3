package com.android.launcher3.util;

public class ThemeConstants {

    /** The system setting for System Themes **/
    public static final String SYSTEM_THEME_STYLE = "system_theme_style";

    public static final int AUTO = 0;
    public static final int LIGHT_THEME = 1;
    public static final int DARK_THEME = 2;
    public static final int BLACK_THEME = 3;
}
